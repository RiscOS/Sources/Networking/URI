/* Copyright 1998 Acorn Computers Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* -*-C-*-
 *
 * $Header$
 * $Source$
 *
 * Copyright(c) 1995 Acorn Computers Ltd., Cambridge, England
 *
 * $Log$
 * Revision 1.1  95/01/04  09:57:07  kwelton
 * Initial revision
 *
 */
#ifndef __size_t
# define __size_t 1
  typedef unsigned int size_t;   /* from <stddef.h> */
#endif

#ifdef DEBUGr
# define rmalloc Drmalloc
# define rfree Drfree
# define rrealloc Drrealloc
# define rcalloc Drcalloc
# define rfreecall Drfreeall
# define rstrdup Drstrdup
# define rcheckcall Drcheckall
#endif

void *rmalloc(size_t);
void rfree(void *);
void *rrealloc(void *,size_t);
void *rcalloc(size_t,size_t);
void rfreeall(void);
char *rstrdup(const char *);
int rcheckall(void);

void roverwritten(void *err);

extern int rallocdebug;
extern int do_not_mix_debugging_ralloc;

#ifndef NORALLOCDEFINES
# define malloc(n) rmalloc(n)
# define free(p) rfree(p)
# define realloc(p,n) rrealloc(p,n)
# define calloc(n,m) rcalloc(n,m)
# define freeall() rfreeall()
# define strdup(s) rstrdup(s)
#endif

/* EOF ralloc.h */
