# Copyright 1998 Acorn Computers Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Makefile for AcornURI
#

COMPONENT    = URI
OBJS         = globals module ralloc task URIswis \
               cstart robase
CINCLUDES    = -Itbox:,C:
HDRS         = URI
ASMHDRS      = URI
CMHGAUTOHDR  = URI
CMHGDEPENDS  = module globals task
CUSTOMEXP    = custom
LIBS         = ${WIMPLIB}
LIBEXT       = o
ROMCDEFINES  = -DROM
ifeq ("${CMDHELP}","None")
CMHGDEFINES += -DNO_INTERNATIONAL_HELP
endif

include CModule

# Sidestep name clash in WimpLib and RISC_OSLib
ABSSYM       = ${C_ABSSYM}

# This module outputs a library file too
export_libs_custom: veneers.o
	${AR} ${ARFLAGS} <CExport$dir>.${LIBEXT}.URI veneers.o
	@${ECHO} ${COMPONENT}: library export complete
export_hdrs_custom: export_hdrs_
	@${NOP}

# Dynamic dependencies:
